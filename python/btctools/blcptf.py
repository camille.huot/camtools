import json
import urllib.request
from decimal import Decimal

api = 'http://in-solidstate.rhcloud.com/blc/api'
myurl = api + '/users/1356/investments'
myptf = json.loads(urllib.request.urlopen(myurl).read().decode('utf-8'))
usdrate = 477 # $477 for 1btc

totalfunding = 0
totalfunded = 0

funded = {}
funding = {}
both = {}
funding = {}
for invest in myptf:
 invest['loan'] = json.loads(urllib.request.urlopen(api + invest['links']['href']).read().decode('latin1'))
 if invest['loanStatus'] == 'Funding':
  if not invest['loan']['user'] in funding:
   funding[invest['loan']['user']] = []
  funding[invest['loan']['user']].append(invest)
  if not invest['loan']['user'] in both:
   both[invest['loan']['user']] = []
  both[invest['loan']['user']].append(invest)
 elif invest['loanStatus'] == 'Funded':
  if not invest['loan']['user'] in funded:
   funded[invest['loan']['user']] = []
  funded[invest['loan']['user']].append(invest)
  if not invest['loan']['user'] in both:
   both[invest['loan']['user']] = []
  both[invest['loan']['user']].append(invest)
 elif invest['loanStatus'] == 'Repaid/Closed':
  pass

fmt = "{:20} -> {:3.8f} BTC ({:3.2f}%)"
totalfunded = sum([ sum([ x['amount'][-1] == 'C' and Decimal(x['amount'].replace(' BTC','')) or x['amount'][-1] == 'D' and Decimal(x['amount'].replace(' USD',''))/usdrate or 0 for x in funded[b] ]) for b in funded.keys() ])
totalfunding = sum([ sum([ x['amount'][-1] == 'C' and Decimal(x['amount'].replace(' BTC','')) or x['amount'][-1] == 'D' and Decimal(x['amount'].replace(' USD',''))/usdrate or 0 for x in funding[b] ]) for b in funding.keys() ])

# prints borrowers from the biggest to the smallest
print("STATUS FUNDING:")
for borrower in sorted(funding, key=lambda b: sum([ x['amount'][-1] == 'C' and Decimal(x['amount'].replace(' BTC','')) or x['amount'][-1] == 'D' and Decimal(x['amount'].replace(' USD',''))/usdrate or 0 for x in funding[b] ]), reverse = True):
 bsum = sum([ x['amount'][-1] == 'C' and Decimal(x['amount'].replace(' BTC','')) or x['amount'][-1] == 'D' and Decimal(x['amount'].replace(' USD',''))/usdrate or 0 for x in funding[borrower] ] )
 print(fmt.format(borrower, bsum, 100 * bsum / totalfunding))

print("STATUS FUNDING+FUNDED:")
for borrower in sorted(both, key=lambda b: sum([ x['amount'][-1] == 'C' and Decimal(x['amount'].replace(' BTC','')) or x['amount'][-1] == 'D' and Decimal(x['amount'].replace(' USD',''))/usdrate or 0 for x in both[b] ]), reverse = True):
 bsum = sum([ x['amount'][-1] == 'C' and Decimal(x['amount'].replace(' BTC','')) or x['amount'][-1] == 'D' and Decimal(x['amount'].replace(' USD',''))/usdrate or 0 for x in both[borrower] ] )
 print(fmt.format(borrower, bsum, 100 * bsum / (totalfunding+totalfunded)))

print("STATUS FUNDED:")
for borrower in sorted(funded, key=lambda b: sum([ x['amount'][-1] == 'C' and Decimal(x['amount'].replace(' BTC','')) or x['amount'][-1] == 'D' and Decimal(x['amount'].replace(' USD',''))/usdrate or 0 for x in funded[b] ]), reverse = True):
 bsum = sum([ x['amount'][-1] == 'C' and Decimal(x['amount'].replace(' BTC','')) or x['amount'][-1] == 'D' and Decimal(x['amount'].replace(' USD',''))/usdrate or 0 for x in funded[borrower] ] )
 print(fmt.format(borrower, bsum, 100 * bsum / totalfunded))

print("TOTAL FUNDING: {:f} BTC".format(totalfunding))
print("TOTAL FUNDING+FUNDED: {:f} BTC".format(totalfunding+totalfunded))
print("TOTAL FUNDED: {:f} BTC".format(totalfunded))
