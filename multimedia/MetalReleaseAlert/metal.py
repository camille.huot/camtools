#!/usr/bin/env python3


favs = []
def load_favorites():
  global favs
  with open('favorites.txt', 'r') as f:
    favs = f.read().splitlines()

from lxml import html
import requests
releases = []
def scrap_metalstorm():
  global releases
  for url in ['http://metalstorm.net/events/new_releases.php',]:
    page = requests.get(url)
    tree = html.fromstring(page.content)
    scrap = tree.xpath('//*[@id="page-content"]/div[4]/div/div/div[2]/div[1]/div[1]')
    for rel in scrap:
      band = rel.xpath('span[1]/a[1]/text()')[0]
      album = rel.xpath('span[1]/a[2]/text()')[0]
      reldate = rel.xpath('span[last()]/text()')[0]
      releases.append((band, album, reldate),)

good = []
def filter():
  global favs, releases, good
  #print(favs)
  for rel in releases:
    #print(rel)
    if rel[0] in favs:
      good.append(rel)

import smtplib
from email.mime.text import MIMEText
def alert():
  msg = MIMEText('''
Here is your monthly alert for metal releases:
{}
Enjoy!
'''.format('\n'.join([ '- {2}: {0} - {1}'.format(*rel) for rel in good ])))
  msg['Subject'] = 'Metal releases weekly update'
  msg['From'] = 'metal@cameuh.net'
  msg['To'] = 'camille.huot@gmail.com'
  s = smtplib.SMTP('localhost')
  s.send_message(msg)
  s.quit()


def main():
  load_favorites()
  scrap_metalstorm()
  filter()
  alert()

if __name__ == '__main__':
  main()
