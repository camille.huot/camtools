@echo off

rem tompeg2 is part of code.google.com/camtools, copyright 2011 Camille Huot
rem and is licensed under http://www.gnu.org/copyleft/gpl.html

rem tompeg2 is a simple BAT script for Windows, to convert a video to MPEG2.
rem I use it to convert the MOV/MJPEG/PCM file created by my camera in order
rem to edit it.


rem define output parameters
set vb=8000
set ab=160
set vlc="C:\Program Files (x86)\VideoLAN\VLC\vlc.exe"

if %1 == "" goto usage_exit

echo Converting file to MPEG2/MP3
echo.
echo Video bitrate: %vb%
echo Audio bitrate: %ab%
echo.
echo File to be converted:
echo.
echo %1
echo.
echo.
echo Starting VLC...
%vlc% %1 --sout=#transcode{vcodec=mp2v,vb=%vb%,scale=1,acodec=mpga,ab=%ab%,channels=2,samplerate=44100}:file{dst='%1.mpg'}
goto exit

:usage_exit
echo You have to drag and drop a file on me!
echo.
pause
goto exit

:exit
