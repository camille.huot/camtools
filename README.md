camtools is a suite of scripts written for my personal usage and licensed under GPL so everyone could make use of them under the terms of the license.

It includes various tools, not limited to:

- unix administration
- multimedia conversion
- object classes for various languages
- multimedia files management