#!/bin/bash

# check_os_updates is part of code.google.com/p/camtools,
# Copyright 2006 Camille Huot,
# Licensed under http://www.gnu.org/copyleft/gpl.html

# script pour automatiser le "apt-get upgrade -s" sur le parc d'une prod

# config des prods
icom=13425
srv[icom]="icom03wb.b1.woo icom04wb.b1.woo icom05sv.b1.woo icom53tr.b1.woo \
icom06si.b1.woo icom07si.b1.woo icom08tr.b1.woo icom09tr.b1.woo \
icom10fs.b1.woo icom11fs.b1.woo icom54si.b1.woo icom55fs.b1.woo \
icom12ms.b1.woo icom13ms.b1.woo ims-pmod1.b1.woo ims-pfw1.b1.woo \
ims-jp1.ftmms ims-jp2.ftmms ims-jp3.ftmms ims-mod4.b1.woo ims-mod5.b1.woo \
ims-mod6.b1.woo icom24im.b1.woo icom10ql.b1.woo icom57im.b1.woo \
ora50.olink.b1.p.fti.net \
icom01wb.b1.woo icom02wb.b1.woo icom01ql.b1.woo icom50dv.b1.woo \
icom18cc.b1.woo \
icom19wb.b1.woo icom20wb.b1.woo icom21ql.b1.woo \
vsio97me.b2.woo icom05ql.b2.woo icom06ql.b2.woo icom02ql.b2.woo"
mail[icom]="camille.huot@wanadooportails.com"

usag=23547
srv[usag]="usag01wb.b2.woo usag02wb.b2.woo usag03ap.b2.woo usag04fs.b2.woo \
usag05im.b2.woo usag06mx.b2.woo usag07so.b2.woo usag12ql.b2.woo"
mail[usag]="camille.huot@wanadooportails.com"

owl=456
srv[owl]="web01.owl.b1.p.fti.net web02.owl.b1.p.fti.net \
sql01.owl.b1.p.fti.net sql02.owl.b1.p.fti.net"
mail[owl]="camille.huot@wanadooportails.com"

function check_prod() {
 grep -q "srv\[$1\]=" "$0"
}

function list_prods() {
 grep "^srv\[.*\]=" "$0" | sed -e 's/^.*\[//' -e 's/\].*$//' | tr '\n' ' '
}

function usage() {
 echo
 echo " $(basename $0) fait un reporting automatique sur les prods specifiees
 et donne la liste des paquets a mettre a jour sur la distribution installee en
 se connectant en ssh et en executant un apt-get dessus. Voici la liste des
 productions supportees par votre script :"
 for p in $(list_prods); do
  echo "   - $p"
 done
 echo "Usage: $0 [-m] [-a] [-h] [<prod>...]"
 echo " -m : envoie le resultat par mail"
 echo " -a : passe sur toutes les prods configurees"
 echo " -h : affiche cette aide"
 echo "prod : une prod parmi celle(s) affichee(s) ci-dessus"
 echo
 exit
}

function send() {
 if [ "$_mail" = "1" ]; then
  echo -e "$text" | mail -s "Rapport des mises a jour a faire sur la production $title" ${mail[$title]}
 else
  echo "*** Rapport des mises a jour a faire sur la production $title ***"
  echo -e "$text"
 fi
}

function check_os() {
 local prod="$1"
 # qq variables temp
 local srvtoupdate="" srvok="" srvko=""

 for server in ${srv[$prod]}; do
  out="$(ssh $server apt-get -qqy update 2>&1)"
  ret="$?"
  if [ "$ret" != "0" ]; then
   srvko=$srvko" * $server *\n"
   srvko=$srvko"\nProbleme :\n${out}\n\n"
   continue
  fi

  out="$(ssh $server apt-get upgrade -s | grep ^Inst)"
  if [ "$(echo -n $out | wc -c)" = "0" ]; then
   srvok="${srvok}$server\n"
  else
   srvtoupdate="${srvtoupdate} * $server *\n${out}\n\n"
  fi
 done

 echo "Liste des paquets a mettre a jour sur les serveurs $prod"
 echo ""

 if [ ! -z "$srvko" ]; then
  echo "Les machines suivantes ne repondent pas correctement :"
  echo ""
  echo -e "$srvko"
 fi

 if [ -z "$srvtoupdate" ]; then
  echo "Aucune machine a mettre a jour."
 else
  echo "Les machines suivantes doivent etre mises a jour :"
  echo ""
  echo -e "$srvtoupdate"
 fi

 if [ ! -z "$srvok" ]; then
  echo "Les machines suivantes sont correctement a jour :"
  echo -e "$srvok"
 fi

 echo "Powered by Camille."
}

### main

TEMP=$(getopt -o amh -- "$@")
eval set -- "$TEMP"

_send_mail=0
_check_all=0
while true; do
 case "$1" in
  -m) _send_mail=1; shift;;
  -a) _check_all=1; shift;;
  -h) usage; shift;;
  --) shift; break;;
 esac
done

if [ "$_check_all" = "1" ]; then
 for p in $(list_prods); do
  reporting=$(check_os $p)
  _mail="$_send_mail" text="$reporting" title="$p" send
 done
else
 if [ -z "$*" ]; then
  echo "[!] Pas d'argument."
  usage
  exit
 fi
 for arg in $*; do
  if ! check_prod $arg; then
   echo "$arg n'est pas une prod valide."
  else
   reporting=$(check_os $arg)
   _mail="$_send_mail" text="$reporting" title="$arg" send
  fi
 done
fi
