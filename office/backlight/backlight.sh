#!/bin/bash

Usage() {
 echo "backlist.sh <up|down>"
 exit 0
}

itv=60
filemax=/sys/class/backlight/intel_backlight/max_brightness
filecur=/sys/class/backlight/intel_backlight/actual_brightness
filenew=/sys/class/backlight/intel_backlight/brightness

Up() {
 max=$(< ${filemax})
 cur=$(< ${filecur})
 new=$(( $cur + $itv ))
 [ "${new}" -gt "${max}" ] && new="${max}"
 if [ "${new}" -ne "${cur}" ]; then
  echo "${new}" | sudo tee "${filenew}"
 fi
}

Down() {
 min=1
 cur=$(< ${filecur})
 new=$(( $cur - $itv ))
 [ "${new}" -lt "${min}" ] && new="${min}"
 if [ "${new}" -ne "${cur}" ]; then
  echo "${new}" | sudo tee "${filenew}"
 fi
}

case "$1" in
 up) Up ;;
 down) Down ;;
 *) Usage ;;
esac
