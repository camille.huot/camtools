<?php
#phpinfo(); exit;

/*
Un peu de documentation :

Pour installer Travail, il faut un PHP4 qui supporte DBX *ET* MySQL et une base
MySQL. Le fichier .php peut-�tre plac� n'importe o� et avoir n'importe quel nom.

Pour installer la base MySQL, en administrateur MySQL :

CREATE DATABASE travail;
USE travail;

CREATE TABLE t_projects (
	id INT AUTO_INCREMENT PRIMARY KEY,
	label VARCHAR(255) UNIQUE NOT NULL,
	color VARCHAR (8) DEFAULT '#000000' NOT NULL
);
CREATE TABLE t_tasks (
	id INT AUTO_INCREMENT PRIMARY KEY,
	id_project INT NOT NULL,
	person VARCHAR(64),
	text BLOB NOT NULL,
	done TINYINT DEFAULT 0
);

GRANT SELECT,INSERT,DELETE,UPDATE ON travail.* TO 'travailuser'@'localhost' \
	IDENTIFIED BY 'travailpass';

Voici les variables � modifier en fonction de votre installation MySQL :
*/

define('SQLHOST', 'localhost');
define('SQLUSER', 'travailuser');
define('SQLPASS', 'travailpass');
define('SQLBASE', 'travail');
define('SQLPREF', 't');
define('COOKIENAME', 'travail_name');






### ne plus toucher ###

error_reporting(E_ALL);
$errors = array();
$myname = NULL;

function debug($var) {
	echo '<pre>'; print_r($var); echo '</pre>';
}
#debug($_SERVER); exit;

/* Ai-je un nom ? */
if (isset($_COOKIE['travail_name'])) {
	$myname = $_COOKIE['travail_name'];
}

/* Est-ce qu'on a chang� mon nom ? */
if (isset($_REQUEST['newname'])) {
	$myname = $_REQUEST['newname'];
}

/* Est-ce que j'ai perdu mon nom ? */
if (isset($_REQUEST['logoff'])) {
	setcookie('travail_name', FALSE);
	$myname = NULL;
}

/* Mise � jour du cookie */
if ($myname) {
	setcookie('travail_name', $myname, time()+60*60*24*30*2); # deux mois
}

/* handler mysql */
$db = @dbx_connect(DBX_MYSQL, SQLHOST, SQLBASE, SQLUSER, SQLPASS);
if (!$db) {
	echo 'Impossible d\'ouvrir la base de donn�es.';
	exit;
}

/* Faut-il creer une tache ? */
$project = 0;
if (isset($_REQUEST['newtask']) && isset($_REQUEST['project']) && isset($_REQUEST['newproject']) && isset($_REQUEST['content'])) {
	if ($_REQUEST['newproject'] != '') {
		$sql = 'INSERT INTO '.SQLPREF.'_projects (label, color) VALUES (\''.addslashes($_REQUEST['newproject']).'\', \'#000000\')';
		if (!dbx_query($db, $sql)) {
			$errors[] = "Erreur pendant '".$sql."'.";
		} else {
			$sql = 'SELECT id FROM '.SQLPREF.'_projects WHERE label=\''.addslashes($_REQUEST['newproject']).'\'';
			$result = dbx_query($db, $sql);
			$project = intval($result->data[0]['id']);
		}
	} else {
		$project = intval($_REQUEST['project']);
	}
	if (!$project) {
		$errors[] = 'Impossible de d�terminer le projet auquel il faut ajouter une t�che.';
	} else {
		$sql = 'INSERT INTO '.SQLPREF.'_tasks (id_project, text) VALUES (\''.$project.'\', \''.addslashes($_REQUEST['content']).'\')';
		if (!dbx_query($db, $sql)) {
			$errors[] = "Erreur pendant '".$sql."'.";
		}
	}
}

/* Me suis-je assign� une t�che ? */
if (isset($_REQUEST['take']) && isset($_REQUEST['task'])) {
	$task = intval($_REQUEST['task']);
	if (!$myname) {
		$errors[] = 'Il faut vous identifier pour prendre une t�che.';
	} elseif ($task == 0) {
		$errors[] = 'Impossible de d�terminer quelle t�che assigner.';
	} else {
		$sql = 'UPDATE '.SQLPREF.'_tasks SET person=\''.addslashes($myname).'\' WHERE id='.$task;
		if (!dbx_query($db, $sql)) {
			$errors[] = "Erreur pendant '".$sql."'.";
		}
	}
}

/* Ai-je termin� une t�che ? */
if (isset($_REQUEST['close']) && isset($_REQUEST['task'])) {
	$task = intval($_REQUEST['task']);
	if (!$myname) {
		$errors[] = 'Il faut vous identifier pour clore une t�che.';
	} elseif ($task == 0) {
		$errors[] = 'Impossible de d�terminer quelle t�che clore.';
	} else {
		$sql = 'SELECT person FROM '.SQLPREF.'_tasks WHERE id='.$task;
		if (!($result = dbx_query($db, $sql))) {
			$errors[] = "Erreur pendant '".$sql."'.";
		} elseif ($result->data[0]['person'] == $myname) {
			$sql = 'UPDATE '.SQLPREF.'_tasks SET done=1 WHERE id='.$task;
			if (!dbx_query($db, $sql)) {
				$errors[] = "Erreur pendant '".$sql."'.";
			}
		} else {
			$errors[] = 'Il faut prendre la t�che avant de pouvoir la clore.';
		}
	}
}

/* Dois-je modifier une t�che ? */
if (isset($_REQUEST['changetask']) && isset($_REQUEST['task'])) {
	$task = intval($_REQUEST['task']);
	if ($task == 0) {
		$errors[] = 'Impossible de d�terminer quelle t�che clore.';
	} else {
		$sql = 'UPDATE '.SQLPREF.'_tasks SET text=\''.addslashes($_REQUEST['changetask']).'\' WHERE id='.$task;
		if (!dbx_query($db, $sql)) {
			$errors[] = "Erreur pendant '".$sql."'.";
		}
	}
}


/* Affichage de la liste */

# fetch des projets
class Project {
	var $label;
	var $color;
	function Project($label, $color) {
		$this->label = $label;
		$this->color = $color;
	}
}
$result = dbx_query($db, 'SELECT id,label,color FROM '.SQLPREF.'_projects');
$projects = array();
foreach ($result->data as $project) {
	$new = new Project($project['label'], $project['color']);
	$projects[$project['id']] = $new;
}

# fetch des taches
$result = dbx_query($db, 'SELECT id,id_project,person,text FROM '.SQLPREF.'_tasks WHERE done=0');
$tasks = $result->data;
?>
<?php echo '<?xml version="1.0" encoding="iso-8859-15"?>'; ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
<title>Travail</title>
<meta http-equiv="cache-control" content="no-cache"/>
<style type="text/css">
table {
	width: 100%;
}
th, td {
	border: solid thin black;
	text-align: center;
}
ul#errorlist li {
	text-color: red;
	font-weight: bold;
}
div#login_info {
	position: absolute;
	top: 1em;
	right: 2em;
}
</style>
<script type="text/javascript">
function submitModify(form) {
	document.forms[form].submit();
	return true;
}
</script>
</head>
<body>

<h1>Travail <small><small><a href="<?php echo $_SERVER['REDIRECT_URL']; ?>">Actualiser</a></small></small></h1>

<?php
if (count($errors)) { ?>
<h2>Erreur(s) lors du traitement&nbsp;:</h2>
<ul id="errorlist">
<?php
foreach ($errors as $error) { ?>
<li><?php echo $error; ?></li>
<?php } ?>
</ul>
<?php } ?>

<table>
<tr>
 <th style="width: 15%">Personne qui s'en occupe</th>
 <th style="width: 20%">Projet</th>
 <th style="width: 45%">� faire</th>
 <th style="width: 20%">Actions</th>
</tr>

<?php
if (count($tasks) == 0) { ?>
<tr><td colspan="4">Rien � faire pour le moment.</td></tr>
<?php } else foreach ($tasks as $task) { ?>
<tr>
 <td><?php
if (empty($task['person'])) {
	echo '-';
} else {
	echo stripslashes($task['person']);
}
?></td>
 <td><?php echo stripslashes($projects[$task['id_project']]->label); ?></td>
 <td>
  <form action="<?php echo $_SERVER['REDIRECT_URL']; ?>" id="modify<?php echo $task['id']; ?>">
  <p>
  <textarea name="changetask" cols="100%" rows="4"<?php if (!$myname || !empty($task['person'])) echo ' readonly="readonly"'; ?>><?php echo stripslashes($task['text']); ?></textarea>
  <input type="hidden" name="task" value="<?php echo $task['id']; ?>"/>
  </p>
  </form>
 </td>
 <td>
<?php
if ($myname) {

if (empty($task['person'])) { ?>
<form action="">
<p>
<input type="button" value="Modifier" onclick="return submitModify('<?php echo 'modify'.$task['id']; ?>')"/>
</p>
</form>
<?php }
if (($myname == $task['person'])) { ?>
<form action="">
<p>
<input type="submit" name="close" value="J'ai fini"/>
<input type="hidden" name="task" value="<?php echo $task['id']; ?>"/>
</p>
</form>
<?php } else { ?>
<form action="">
<p>
<input type="submit" name="take" value="Je prends"/>
<input type="hidden" name="task" value="<?php echo $task['id']; ?>"/>
</p>
</form>
<?php }

} else { echo '<small>(il faut s\'identifier pour faire une action)</small>'; } ?>
 </td>
</tr>

<?php } ?>

</table>


<p>&nbsp;</p>
<hr/>
<h3>Nouvelle t�che</h3>

<form action="">
<p>
Projet&nbsp;:
<select name="project">
<?php
foreach ($projects as $id => $project) { ?>
<option value="<?php echo $id; ?>"><?php echo stripslashes($project->label); ?></option>
<?php } ?>
</select><br/>
Si nouveau projet&nbsp;: <input type="text" name="newproject"/><br/>
Description de la t�che&nbsp;:<br/>
<textarea name="content" cols="100%" rows="10"></textarea><br/>
<input name="newtask" type="submit"/>
</p>
</form>

<div id="login_info">
<form action="">
<?php
if ($myname) {
?>
<p>
<input type="submit" name="logoff" value="Log off <?php echo $myname; ?>"/>
</p>
<?php } else { ?>
Veuillez vous identifier&nbsp;: <input type="text" name="newname" />
<?php } ?>
</form>
</div>

<p>&nbsp;</p>
<hr/>
</body>
</html>
